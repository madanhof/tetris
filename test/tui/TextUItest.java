package tui;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class TextUItest {
	
	TextUI test;
	@Before
	public void setUp() throws Exception {
		test = new TextUI();
	}

	@Test
	public void testBooleanTest() {
		assertTrue(test.booleantest(1));
		assertFalse(test.booleantest(0));
		assertEquals(null, test.booleantest(123));
	}
	
	@Test
	public void testBooleanTesta() {
		assertTrue(test.booleantesta(1));
		assertFalse(test.booleantesta(0));
		assertEquals(null, test.booleantesta(123));
	}

}
